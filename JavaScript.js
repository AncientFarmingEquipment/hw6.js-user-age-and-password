const person = createNewPerson();
function checkCorrectness(message) {
    let txtString;

    do {
        txtString = prompt(message);
    } while (txtString === null || txtString === "")

    return txtString;
}

function dateParser(input) {
    let date = input.split('.');
    return (new Date(date[2], date[1] - 1, date[0]));
}

function createNewPerson() {
    return {
        _firstName: checkCorrectness("Enter your name"),
        _lastName: checkCorrectness("Enter your last name"),
        _birthday: dateParser(prompt(`Enter your birthday (dd.mm.yyyy)`)),

        set firstName(value) {
            this["_firstName"] = value;
        },

        get firstName() {
            return this._firstName;
        },

        set lastName(value) {
            this["_lastName"] = value;
        },

        get lastName() {
            return this._lastName;
        },

        getLogin() {
            return (this._firstName[0] + this._lastName).toLowerCase();
        },

        getAge() {
            return parseInt(((new Date().getTime() - this._birthday.getTime()) / (365.25 * 3600 * 24 * 1000)).toString());
        },

        getPassword() {
            return (this._firstName[0].toUpperCase() + this._lastName.toLowerCase() + this._birthday.getFullYear());
        },
    };
}
console.log(person);
console.log(person.getLogin());
console.log(person.getAge());
console.log(person.getPassword());
console.log(person._birthday);
